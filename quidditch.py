#open files
list1 = open("registration_list1.txt")
list2 = open("registration_list2.txt")
##initialize variables for the player numbers
#function to go through a file and print results
print("Files opened correctly")
def register_file(registrationlist):
    canplay = 1
    h = [0, "Hufflepuff"]
    g = [0, "Gryffindor"]
    r = [0, "Ravenclaw"]
    s = [0, "Slytherin"]
    for line in registrationlist:
        if "Gryffindor" in line:
            g[0] += 1
        if "Slytherin" in line:
            s[0] += 1
        if "Hufflepuff" in line:
            h[0] += 1
        if "Ravenclaw" in line:
            r[0] += 1
    houselist = [g, r, s, h]
    for House in houselist:
        if House[0] > 7:
            canplay = 0
            print(House[1] + " has too many players.")
        elif(House[0] < 7): 
            print(House[1] + " has not enough players.")
            canplay = 0
    if canplay == 1:
        print("List complete, let's play quidditch.")
        
register_file(list1)
print("")
register_file(list2)


