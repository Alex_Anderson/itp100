def plurality(election):
    """
    Return the plurality winner among three candidates, 1, 2, and 3. The input
    election is a list of three tuples, so in a plurality election only the
    first element matters.

      >>> plurality([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
      2
      >>> plurality([(3, 2, 1), (3, 2, 1), (2, 1, 3)])
      3
    """
    # Count the votes for each of the three candidates in a dictionary
    vote_totals = {1: 0, 2: 0, 3: 0} 
    for vote in election:
        vote_totals[vote[0]] += 1

    # Return the candidate number with the highest vote count
    return max(vote_totals.items(), key=lambda x: x[1])[0]

def exhaustive(election):
    """ There needs to be a candidate with a majority of the votes, so one has to repeat the process until
        one has majority of votes
        
        >>> exhaustive([(1,2,3),(2,3,1),(1,2,3),(2,1,3),(1,2,3),(2,1,3),(3,2,1),(3,2,1)])
        2
        >>> exhaustive([(2,1,3),(3,2,1),(1,3,2),(2,3,1),(3,2,1)])
        3
    """
    vote_totals = {1: 0, 2: 0, 3: 0}
    amountofvotes = 0
    for vote in election:
        amountofvotes += 1
        vote_totals[vote[0]] += 1
    if vote_totals[max(vote_totals.items(), key=lambda x: x[1])[0]] > amountofvotes:
        return(max(vote_totals.items(), key=lambda x: x[1])[0])
    else:
        castout = min(vote_totals.items(), key=lambda x: x[1])[0]
        vote_totals = {1:0, 2:0, 3:0}
        del vote_totals[castout]
        for vote in election:
            if vote[0] != castout:
                vote_totals[vote[0]] += 1
            else:
                vote_totals[vote[1]] += 1
        return max(vote_totals.items(), key=lambda x: x[1])[0]

def onetwoprimary(election, last):
    """ face one and two than winner goes against three
    
        >>> onetwoprimary([(1,2,3),(2,3,1),(2,3,1),(3,2,1)])
        2
        >>> onetwoprimary([(1,2,3),(1,2,3),(2,1,3),(3,1,2),(3,2,1),(3,2,1),(3,1,2)])
        3
    
    """
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election: 
        if vote[0] == last:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1
    winner = max(vote_totals.items(), key = lambda x: x[1])[0]
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        if vote[0] != winner and vote[0] != last:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1
    return max(vote_totals.items(), key = lambda x: x[1])[0] 



if __name__ == "__main__":
    import doctest
    doctest.testmod()

