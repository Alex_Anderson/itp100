i = open("gene_splicing_test.dat")
line1 = i.readline()
print("This is the amount of cases")
print(line1)
 #get past the first line
#we can now iterate through the date we actually need
import time
for line in i:
    abct = line.split()
    iterations = 0
    a = abct[0]
    b = abct[1]
    c = abct[2]
    t = abct[3]
     #print(" A:" + a + " b:" + b + " c:" + c + " t:" + t)
     #used to check if they all got the right parts of the lind, they did
     #convert to float for calculation
    a = float(a)
    b = float(b)
    c = float(c)
    t = float(t)
         #[a,b]
    #get target 
    target = a/b
    dish1 = [a,0]
    dish2 = [0,b]
    #complete is boolean value, 0 = false 1 = true
    #using this as the while loop for easier readability
    complete = 0
    mix1 = 0
    mix2 = 0
    while complete != 1:
        iterations += 1
        dish2[0] += dish1[0]*(c/(dish1[0]+dish1[1]))
        dish2[1] += dish1[1]*(c/(dish1[0]+dish1[1]))
        #get the constant for the dishes to mix
        constant = dish1[0]+dish1[1] 
        #
        dish1[0] -= dish1[0]*(c/(constant))
        dish1[1] -= dish1[1]*(c/(constant))
        #take from dish 2 mix iwth dish1
        dish1[0] += dish2[0]*(c/(dish2[0]+dish2[1]))
        dish1[1] += dish2[1]*(c/(dish2[0]+dish2[1]))
        #get the constant to take away from dishes to divide by
        constant = dish2[0]+dish2[1]
        #
        dish2[0] -= dish2[0]*(c/(constant))
        dish2[1] -= dish2[1]*(c/(constant))
        #adding MUST come before subtracting to work
        mix1 = dish1[0]/dish1[1]
        mix2 = dish2[0]/dish2[1]
        if mix1 != 0 and mix2 != 0:
            if abs(mix1-target) <= t and abs(mix2-target) <= t:
                complete = 1

        
    print(iterations)   


