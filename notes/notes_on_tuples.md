
1. Yes, they are like lists in the aspect that they contain items in a 1 dimensional
array, and they can be iterated through.

2. Tuples are immutable. You can chagne the contents after it is created. It is not 
modifiable because they are quicker to access and take up less storage - they're 
more optimized.

3.
factorsOf3 = (1, 3)
(firstFactorOf3, secondFactorOf3) = (1, 3)
Tuple assignments seems convenient because you can use it to assign multiple variables
different values at once.

4.
alphabet = {'a':1, 'b':2, 'c':3, 'd':4, 'e':5....
letters = list()
for letter, place in alphabet.items():
    letters.append( (place, letter ) )
letters = sorted(letters)
for (place, letter) in letters:
    'Number' print(str(place) + ' in the alphabet is' letter
print("Hope you know the alphabet now!"

5. List comprehension creates a dynamic list -
it is able to create a list of items in a certain format based on the "stamping pattern"
that you tell it to make in the for loop. This means you can sort data into a list value
in different formats using the for loop inside the square brackes ([]) which tells python
it is a list.
