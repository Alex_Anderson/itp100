1. He says we have been talking about algorithims: he defines them as a set of rules or steps to solve a problem.
He defines data structures as a way of organizing data in a computer - like a filter.
2. The collection is a module that provides different types of "containers", which are objects we can use to store
data in different types of structure.
3. It is that even though he writres friends as plural, it is only to be  convenient. The value after in will be 
treated as a list in its "successive" values (e.g. items in a list, characters in a string), so it is important
you organize it correctly since python will not know the difference.
4. Lists are in a array, so we can access values in a list individually by searching the list for the subpositions
in the brackets, and it will retrieve it from that array for us
5. mutable means it can be changed, and lists are mutable since they can be changed (items sorted, appended, removed,
etc.). A immutable data structure cannot be changed, like a string or a integer, because  B will never be b, and
1 will never be 2. However, lists function like an array of variables in a way, so you are assigning these values
a "place" in the list.
6.
Concatenating = using +
"adds" lists together, like a string
[a,b,3] + [c,32] 
would equal[a,b,3,c,32]
Slicing using :
again, functions like a string
t = [1,2]
t[:1]
[1]
7. 
append() adds values
sort() sorts them in order of values
extent() adds things to end of list
insert() adds things in certain positions of the list
reverse() reverses the order of the list
8.  max: finds the largest value
min: finds the smallest value
sum: adds all values of the list
sum/len: use them to get the values of the length and sum and 
perform the equation to the find the average
9.
-we can use split to turn a string into items in a list - we are splitting the "strings" values of putting them in a array
-we can use a double split to get one specific piece of a string by splitting a string, store or use that data of the split string we want
-seperate a string into words in a list by splitting them in their string 
10. The gaurdian pattern is a method in which python will stop at a logical evaluation the moment
it finds a false value, so for an example you can check using a and statement as a way to insure that something
will run, especially if that value would cause the python to stop due to a error (i.e. if y != 0 and
x/y > 2, we are checking if x divided by y equals 2, but we putting  a gaurd if y is zero so it 
automatically marks it as false instead of trying to divice 2 by 0 which would result in a failure.)
