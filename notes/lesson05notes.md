# lesson5notes

Alex Chapter 5 Notes
1.We can use markdown and git to use nice looking documents by using markdown to format the plaintext and
committing them to get where they are turned into html.*EDIT*": Git is the "revision system", since we are
using it to manage changes in the files, sorting each change with the commit and comments of the commit. Gitlab
is able to take our changes and store them online. Git also processes our markdown files into HTML.

2.Functions are a store and reuse pattern, in the sense that they are a pattern of actions that we can tell
the computer to do whenever we call it. We can store the pattern of actions in the function, assign it a name, 
and then call it to tell the computer what we want it to do. Even with parameters there is still a pattern. Besides
the technical part, it is also very convenient, since it allows us to write less code and edit multiple parts of the
program at once versus if we wrote the code of the function individually.

3.We create functions using def. For example, to define the function helloWorld, which prints "hello world", we would do
def helloWorld():
     print("Hello World")
It's a very useless function since it only calls a very simple line of code, unless you are printing hello world a lot and want your program to print something else.

4.When defining the function, it is the store phase, since you are storing the pattern of actions in it, like how you are storing a value in a variable. The process
of using the function to "reuse" that data that tells python to take those actions is calling or invoking the function. Most people use call, but invoke is another way
to think about it, since invoking is to cite something as a authority for an action. We are using the function as the authority to tell python to take those actions.

5.Some built in functions in python serve to change the data type of a value or variable.

favoriteNumber = input("Favorite number?")
addition = int(favoriteNumber) + 5
print("Your favorite number plus five is" + string(favoriteNumber))
Some more are float() or type(). You can also get python libraries and import them 
for other functions.

