1. Why pair program? What benefits do the videos claim for practice?
-can practice both roles
-can learn tips/solutions with things they struggle with from the other role
-learns how to manage your own code across a project
-learn from other people and learn how to work with people and different 
approaches to code than simply your own approaches/logic to solving problems
2. What are the two distinct roles used in the practice of pair programming?
-Driver, typing the code, doing the programming
-Navigator, keeping track of the code being written and telling the driver
which code they should write next, "managing" the driver
It helps to be on the same computer because the navigator can manage the
code of the driver directly and look at the program to offer solutions to
problems they did not fix or more efficient solutions to problems they did 
fix
