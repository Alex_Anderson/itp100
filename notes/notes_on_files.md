#files
# 1. It uses the open function. The arguments, or parameters for the function are the file are the filename and the mode. The mode is optional, using w to write/store data in the file and using r to retrieve data from the file.
# 2. A file handle is a variable defined by the open() function of a file. It is a "label" for the file that allows you to access it.
i# 3. /n is a special character in a string to indicate when a line in a string ends and a new string begins. Newline is one character,
not two.
# 4.The most common way is to tell it to read the first line, then read the next line, then read the next line, etc.
# 5. Yes, becuase /n is one character, so slicing in the reverse direction would work by slicing the newline character off, since it is
is at the end of a line.
# 6.
Method one:
Skipping with continue
fhandle = open('file')
for line in ('fhandle'):
    line = line.rstrip()
    if not line.startwith('From:'):
        contiue
    print(line)
Method two:
Searching through a file
fhandle = open('file')
for line in ('fhandle'):
    line =.line.rstrip()
    if line.startwith('From: ') :
        print(Line)
Method three:
Using in to select files
fhandle = open('file')
for line in ('fhandle'):
    line = line.rstrip()
    if ('From: ') not in line:
        continue
    print(line)
# 7. It is a construct used to deal with a file that isnt there. It does this by acting as a logical operator  to see if the file being there is true or false (except) and the quit function tells the program to close.
