#lesson5notes
1 The other patterns are sequential, conditional, store and reuse.
2 It counts down to 0 by subtracting 1 from the variable each loop. It "counts" it by
printing the variable each loop, telling us what "second" it is on 
3 repeated steps/iteration
4 It's a infinite loop, which is caused by the iteration never changing, so the condition for
the while loop is always running. 
5 the break statement can be used to exit a loop, and it does it by telling python to stop whichever
"section" of code it is in. You can also use it in funcitons.
6 It is a zero trip loop, which functions more like a if statement. It is mostly used for
python to not run it, so you can use it to troubleshoot your code by putting it in your code
to run in a condition of the program you do not wish.
7 While is a indefinite loop, because it is a loop that only stops if the condition is not  met.
He says the next loop will be a definite loop.
8 Definite loops iterate through a items of a set- there is a finite set of things. They are
definite loops because they always loop the same number of times, while a while's statement 
length is defined by condition.
9. Loop idioms: what is done within loops
example 1
looping through a set-
processing items indepdently and print them out
looking at which number is the highest by comparing them to the number that came before, and 
storing the number if there is nothing larger, and updating the variable storing that number if
the loop iterates into a higher number.
example 2
summing in a loop =
Taking each item in a loop and adding them together each iteration. E.g., each iteration adds its
number to the sum of the loop.

