def mid_numconditional(n1,n2,n3):
    """
      >>> mid_numconditional(3,8,5)
      5
      >>> mid_numconditional(6,9,4)
      6
      >>> mid_numconditional(7,3,1)
      3
      >>> mid_numconditional(7,7,7)
      7
    """
    middle = 0
    if n1 == n2 and n1 == n3:
        middle = n1
    else:
        if (n1>n2 and n1<n3) or (n1<n2 and n1>n3):
            middle = n1        
        else:
            if (n2>n1 and n2<n3) or (n2<n1 and n2>n3):
                middle = n2
            else:
                middle = n3
    return(middle)

if  __name__ == "__main__":
            import doctest
            doctest.testmod()


