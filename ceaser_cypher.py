import string
lowercaselist = []
for letter in string.ascii_lowercase:
    lowercaselist.append(letter)
print(lowercaselist)
uppercaselist = []
for letter in string.ascii_uppercase:
    uppercaselist.append(letter)
print(uppercaselist)
    
  
#sampletxt
firstsample = open("sample.txt")
cipher = int(firstsample.readline())
encryptedsample = ""
#subtract 1 to align it with the list
for line in firstsample:
    # checking of readline succesfully skips first line print(line)
    for letter in line:
        if letter in lowercaselist:
            if lowercaselist.index(letter) + cipher > 25:
                letter = lowercaselist[lowercaselist.index(letter)+(cipher-26)]
            else:
                letter = lowercaselist[(lowercaselist.index(letter)+cipher)]
        if letter in uppercaselist:
            if  uppercaselist.index(letter) + cipher > 25:
                letter = uppercaselist[uppercaselist.index(letter)+(cipher-26)]
            else:
                letter = uppercaselist[(uppercaselist.index(letter)+cipher)]
        encryptedsample = encryptedsample + letter
    encryptedsample = encryptedsample + "\n"
    if "STOP" in line:
        break;
print(encryptedsample)
