def middle_value(n1,n2,n3):
    """
      >>> middle_value(3,8,5)
      5
      >>> middle_value(6,9,4)
      6
      >>> middle_value(3,7,1)
      3
      >>> middle_value(7,7,7)
      7
    """
    numList = [n1,n2,n3]
    if max(numList) == min(numList):
        return n1
    else:
        highest = max(numList)
        lowest = min(numList)
        numList.remove(highest)
        numList.remove(lowest)
        middle = numList[0]
        return((middle))
                                    
if  __name__ == "__main__":
        import doctest
        doctest.testmod()


